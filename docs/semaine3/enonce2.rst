============
Enonce 2
============

Représentation mémoire
----------------------
On vous donne les listes suivantes :  

* ``l1 = [[1,2],3,[4,5]]``
* ``l2 = [6,7,8]`` 
* ``l3 = [l1[0]+l1[2],l2]`` 
* Représentez les trois listes en mémoire.    
* Quel est l'affichage de ``print(l3)``

Soient les instructions suivantes :
  
* ``l3[0][0] = l1[2]`` 
* ``l1[0][1] = l3[0][0]``

Donnez les affichages correspondant à : 

* ``print(l1)``
* ``print(l2)``
* ``print(l3)`` 

Donnez la représentation mémoire des trois listes.  

Slices
------

A l'aide des résultats obtenus dans l'exercice précédent, donnez les affichages suivants

#. ``print(l1[1:])``
#. ``print(l3[1:2])`` 
#. ``print(l3[0][2:])``
   
Mémoire et fonctions
--------------------
On vous donne les trois fonctions f1, f2, f3 suivantes :

::
   
 def f1(liste) :
    res = []
    for i in range(len(liste)) :
        if i %3 == 0 :
            res.append(liste[i]-1)
        else :
            res.append(liste[i])
 return res
 
 def f2(liste) :
    res = []
    for i in range(len(liste)) :
        if liste[i] % 2 == 0 :
            res.append(liste[i])
 return res
 
 def f3(liste) :
    res = []
    for i in range(len(liste)) :
        if i %2 == 0:
            res.append(liste[i]+1)
        else :
            res.append(liste[i]-1)
    return res

* Dessinez la représentation mémoire des divers appels de fonctions et indiquez ce qu'affiche ``print(f3(f2(f1(l))))``, à l'aide de la liste ``l = [1, 2, 6, 4, 9, 12, 11]``

Animaux et zoo
--------------

On vous propose de représenter un animal sous la forme d'un triplet nom, age et taille. Comment représentez vous un zoo d'animaux ? On désire alors définir certaines fonctions sur un zoo.

#. L'age moyen des animaux du zoo (que modifieriez vous pour donner la taille moyenne). 
#. La liste des animaux dont la taille est inférieure à 10 cm.
#. L'animal ayant la taille la plus petite et s'il y en a plusieurs celui qui est le plus agé.
#. Sauvegarde d'un zoo.
#. Restauration d'un zoo. 
#. Trier les animaux en mettant au début de la liste ceux qui ont un age < à 5 ans. 


.. raw:: latex

    \newpage
