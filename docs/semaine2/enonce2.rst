============
Enonce 2
============
Que fait cette fonction ?
--------------------------

::
   
 def exo1(N) :
    if N == 0 : 
        r = N
    else :
        p = 1
        r = 0
        while N != 0 : 
            r = r*10 + N%10
            N = N // 10
    return r

* Testez cette fonction avec ``4567``,
* puis avec ``11378889``. 
* Que se passe-t-il si le paramètre est nul ? 
* Définissez ce que fait cette fonction. 
* Proposez un invariant de boucle. 

  
Valeurs distinctes d'une liste
------------------------------
On désire connaître les valeurs distinctes d'une liste. Par exemple la liste ``[1, 7, 1, 3, 2, 3, 9, 1]`` contient 5 valeurs distincts qui sont ``[1, 7, 3, 2, 9]``.
Pour cela on vous demande de : 

#. commencer par écrire une fonction permettant de savoir si un nombre appartient à une liste. Vous utiliserez une boucle while dont vous donnerez un invariant. 
#. Utilisez la fonction ci dessus pour déterminer la liste des valeurs distinctes d'une liste. Vous noterez qu'au début, si la liste initiale est non vide, la liste des valeurs distinctes contient la première valeur de la liste ([1] dans l'exemple).  
   
Trier une liste en utilisant le minimum
---------------------------------------

On désire trier une liste dans l'ordre croissant. Nous allons décomposer la démarche :

#. définir une fonction qui extrait d'une liste le minimum. Vous devez construire la liste résultat au fur et à mesure.
#. Modifier cette fonction pour qu'elle fournisse non seulement la liste sans le minimum mais aussi le minimum trouvé. Utilisez un n-uplet sous la forme return (liste, minimum). 
#. Utilisez la fonction précédente pour trier une liste par ordre croissant. Quel est la condition d'arrêt de la boucle de cette fonction, ainsi que son invariant. Pour utiliser un n-uplet de résultats d'une fonction f on écrit (v1, v2) = f(paramètres), v1 est la première valeur du n-uplet, v2 la seconde. 

.. raw:: latex

   \newpage 
