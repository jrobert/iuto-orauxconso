============
Enonce 4
============
Que fait cette fonction ?
--------------------------

::

 def fonc(liste) :
   if len(liste) == 0 : 
      res = None
   else : 
      res = 1 
      m = liste[0]
      for i in range(1, len(liste)) : 
         if liste[i] > m : 
            m = liste[i] 
            res = 1
         else : 
            if m == liste[i] : 
               res = res + 1
   return res 

* Testez cette fonction avec la liste ``[1, 4, 2, 7, 2, 4, 8, 4]``,
* puis avec la liste ``[1, 8, 2, 7, 2, 4, 8, 8]``. 
* Que se passe-t-il si la liste est vide ? 
* Donnez des noms significatifs pour chaque variable.
* Définissez ce que fait cette fonction. 
* Réécrivez la en utilisant une boucle while.
  
Moyenne
-------

On vous demande de définir une fonction permettant de déterminer la moyenne des entiers d'une liste.

Quelle instruction permet de donner  la moyenne de la liste ``[12, -1, 2, 4, 7, -6]`` ? Quel est le résultat ? 

Que se passe-t-il si la liste est vide ?

#. boucle for sur les éléments
#. boucle for avec les indices
#. boucle while
   
Moyenne des multiples de 5
--------------------------
Déterminez la moyenne des entiers multiples de 5 d'une liste d'entiers.
Proposez des exemples, indiquez les paramètres, le résultat et le nom de votre fonction. 

#. boucle for sur les éléments
#. boucle for avec les indices
#. boucle while

Et si on continue
-----------------
On vous demande de calculer la moyenne des valeurs absolues des entiers multiples de 3 d'une liste (3 versions + exemples).

.. raw:: latex

    \newpage
