============
Enonce 3
============
Que fait cette fonction ?
--------------------------

::
   
 def fonc(liste, v, n) :
   c = 0
   bo = False
   for elem in liste :
      if v == elem :
         c = c + 1
         if c >= n :
            bo = True
   return bo 
   
* Testez cette fonction avec la liste ``[1, 4, 2, 7, 2, 4, 8, 4]``, v valant 2 et n valant 2, 
* puis avec la liste ``[1, 4, 2, 7, 2, 4, 8, 4]``, v valant 4 et n valant 5. 
* Que se passe-t-il si la liste est vide, si v vaut 5 et n vaut 2 ? 
* Donnez des noms significatifs pour chaque variable.
* Définissez ce que fait cette fonction. 
* Réécrivez la en utilisant une boucle avec indices, puis une boucle while.

Fréquence
---------

On vous demande de définir une fonction permettant de déterminer la fréquence d'une lettre dans un texte.

Quelle instruction permet de donner la fréquence du caractère u dans la chaîne : bonjour nous sommes le lundi 19 octobre ? Quel est le résultat ? 

Que se passe-t-il si la chaine est vide ?

#. boucle for sur les éléments
#. boucle for avec les indices
#. boucle while

   
Fréquence maximale
------------------

Dans la chaîne : bonjour nous sommes lundi, le caractère le plus fréquent est o.

Dans la chaîne : aa bb cc dd, le caractère le plus fréquent est soit a, soit b, soit c soit d.


Que proposez vous pour la chaîne vide ?

Proposez une fonction fournissant le caractère le plus fréquent dans un texte.

#. boucle for sur les éléments
#. boucle for avec les indices
#. boucle while



   
Et si on continue
-----------------
On désire connaître la liste des caractères les plus fréquents dans un texte.
En vous inspirant des exercices précédents, proposez une telle fonction (3 versions + exemples).

.. raw:: latex

    \newpage


